<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>แสดงข้อมูลแผนก_PDO</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <style>
        body {
            background-color: #f0f0; 
        }

        table {
            width: 100%;
            border-collapse: collapse;
            background-color: white; 
        }

        th,
        td {
            padding: 8px;
            text-align: center;
            border-bottom: 2px solid #f9bdbb;
        }

        th {
            background-color: #333; 
            color: white;
        }

       
        .search-box {
            margin-top: 20px;
            display: flex;
            align-items: center;
        }

        .search-box .form-group {
            margin-right: 10px;
        }

        .container {
            background-color: blanchedalmond; 
            border-radius: 10px; 
            padding: 20px; 
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); 
            margin-top: 30px; 
        }
    </style>
    <script>
        function confirmDelete() {
            return confirm("Are you sure you want to delete this department?");
        }
    </script>
</head>

<body>
     <!-- Navbar -->
     <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://localhost/php2566/LAB-LOGIN/">PDO</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="list_dept_PDO2.PHP"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="ins_dept_PDO.html"><i class="fas fa-plus"></i> Insert</a></li>
                    <li><a href="ins_dept_PDO.html"><i class="fas fa-plus"></i> Search</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fas fa-database"></i> Data
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="../LAB_MOBILE_WEB/list_dept.php">deptSQL</a></li>
                            <li><a href="#">O_N2</a></li>
                            <li><a href="#">O_N3</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


<?php

?>