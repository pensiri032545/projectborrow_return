<?php
include('db.php');
include("function.php");

if(isset($_POST["id"]))
{
	$image = get_image_name($_POST["id"]);
	if($image != '')
	{
		unlink("upload/" . $image);
	}
	$stmt = $connection->prepare(
		"DELETE FROM equipment WHERE id = :bp_id"
	);

	$stmt->bindParam(':bp_id', $_POST["id"]);
	$result = $stmt->execute();
  	
	if(!empty($result))
	{
		echo 'ลบข้อมูลสำเร็จแล้ว !';
	}
}

?>