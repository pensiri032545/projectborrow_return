<?php
// กำหนดค่าการเชื่อมต่อ
$host = "localhost";
$user = "root";
$passwd = "";
$dbname = "project";

// สร้างการเชื่อมต่อ
$conn = new mysqli($host, $user, $passwd, $dbname);

// ตรวจสอบการเชื่อมต่อ
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// รับข้อมูลจากฟอร์ม
$user_name = $_POST["user_name"];
$item_name = $_POST["item_name"];
$return_date = $_POST["return_date"];
$condition = $_POST["condition"];

// เตรียมคำสั่ง SQL สำหรับการแทรกข้อมูล
$sql = "INSERT INTO return_items (user_name, item_name, return_date, condition) 
        VALUES ('$user_name', '$item_name', '$return_date', '$condition')";

// ทำการแทรกข้อมูล
if ($conn->query($sql) === TRUE) {
    echo "Record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

// ปิดการเชื่อมต่อ
$conn->close();
?>
