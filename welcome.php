<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome Page</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Kanit:wght@400;700&display=swap');

        /* ใช้ฟอนต์ Kanit ทั้งหมด */
        body, input, label {
            font-family: 'Kanit', sans-serif;
        }

        body {
            background-image: url('Banner.png'); /* กำหนดภาพพื้นหลัง */
            background-size: cover; /* ให้ภาพพื้นหลังปรับขนาดเพื่อครอบคลุมพื้นที่ของ body */
            text-align: center;
            padding-top: 50px;
            color: #ffffff; /* กำหนดสีข้อความให้เป็นสีขาว */
        }

        h1 {
            color: #333;
        }

        .login-form {
            margin-top: 20px;
            width: 300px;
            margin: 0 auto;
        }

        
        .login-form input[type="submit"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-sizing: border-box;
        }

        .login-form input[type="submit"] {
            width: 100%;
            padding: 10px;
            background-color: #56baed;
            color: #ffffff;
            cursor: pointer;
           
            bottom: 15; /* กำหนดให้ปุ่มอยู่ด้านล่าง */
        }

        .login-form input[type="submit"]:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <?php
    if(isset($_POST['username'])) {
        $username = $_POST['username'];
        echo "<h1>Welcome, $username!</h1>";
    } else {
        echo "<h1></h1>";
    }
    ?>

    <div class="login-form">
        <form action="login_ldap.php" method="post">
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
            <input type="submit" value=" Login Office Equipment Borrowing System">
        </form>
    </div>
</body>
</html>